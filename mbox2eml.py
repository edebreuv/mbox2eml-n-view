# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import re as regx
import sys as sstm
from argparse import ArgumentParser as arg_parser_t
from email.generator import BytesGenerator as byte_generator_t
from email.header import Header as header_t
from io import BytesIO as bytes_io_t
from mailbox import mbox as mbox_t
from pathlib import Path as path_t
from string import ascii_letters, digits

import colorama as clrm
from decoder import FormattedText


VALID_CHARACTERS = ascii_letters + digits + "_-"
INVALID_CHARACTERS_REGX = regx.compile(f"[^{VALID_CHARACTERS}]")


SENDER_RAW = "([^@]+)@.+"
SENDER_ADDRESS = f"<{SENDER_RAW}>"
SENDER_FULL = f"([^<@]+)[^<]*{SENDER_ADDRESS}"  # ([^<@]+)[^<]*: In case display name is actually e-mail address
#
SENDER_RAW_REGX = regx.compile(SENDER_RAW)
SENDER_ADDRESS_REGX = regx.compile(SENDER_ADDRESS)
SENDER_FULL_REGX = regx.compile(SENDER_FULL)
#
MAX_SENDER_LENGTH = 24
UNKNOWN_SENDER = "Unknown_Sender"


DATE_TIME_REGX = regx.compile(
    "(?:[a-zA-Z][a-z]*, +)?"
    "([0-9]{1,2}) +"
    "([a-zA-Z][a-z]*) +"
    "([0-9]{4}) +"
    "([0-9]{1,2}):"
    "([0-9]{2}):"
    "([0-9]{2})"
    "(?: +[+-][0-9]{4,6}(?:\.[0-9]{6})?)?"
    "(?: +\(?[A-Z]{2,4}\)?)?"
)
MONTHS = {
    "ap": 4,
    "au": 8,
    "d": 12,
    "f": 2,
    "ja": 1,
    "jul": 7,
    "jun": 6,
    "mar": 3,
    "may": 5,
    "n": 11,
    "o": 10,
    "s": 9,
}
UNKNOWN_DATE = "Unknown_Date"


MAX_SUBJECT_LENGTH = 12
UNKNOWN_SUBJECT = "Unknown_Subject"


def SenderUsername(match: regx.Match, idx: int, /) -> str:
    """"""
    current = match.group(idx)
    output = current.replace(".", "_").lower()

    return output


def FormattedSender(sender: str, /) -> str:
    """"""
    if sender is UNKNOWN_SENDER:
        return sender

    if (match := SENDER_FULL_REGX.fullmatch(sender)) is None:
        if (match := SENDER_ADDRESS_REGX.fullmatch(sender)) is None:
            if (match := SENDER_RAW_REGX.fullmatch(sender)) is None:
                print(
                    f"{clrm.Fore.RED}{sender}: Unrecognized Sender Format{clrm.Fore.RESET}"
                )
                output = "Unrecognized_Sender_Format"
            else:
                output = SenderUsername(match, 1)
        else:
            output = SenderUsername(match, 1)
    else:
        current = FormattedText(
            match.group(1).strip(), remove_accents=True, only_longest=True
        )
        current = current.replace('"', "").replace(" ", "_").replace(".", "_")
        length_full = current.__len__()
        current = INVALID_CHARACTERS_REGX.sub("", current)
        length_valid = current.__len__()
        if length_valid < length_full:
            output = SenderUsername(match, 2)
        else:
            output = current.lower()

    return output[:MAX_SENDER_LENGTH]


def FormattedDate(
    date: str,
) -> str:  # , /, *, template: str = DATE_TIME_TEMPLATE) -> str:
    """"""
    if date is UNKNOWN_DATE:
        return date

    if (match := DATE_TIME_REGX.fullmatch(date)) is None:
        print(f"{clrm.Fore.RED}{date}: Unrecognized Date Format{clrm.Fore.RESET}")
        output = "Unrecognized_Date_Format"
    else:
        day = int(match.group(1))
        month = match.group(2).lower()
        year = match.group(3)
        hours = int(match.group(4))
        minutes = match.group(5)
        seconds = match.group(6)

        for key, value in MONTHS.items():
            if month.startswith(key):
                month = value
                break
        if isinstance(month, str):
            print(
                f"{clrm.Fore.RED}{date}: Unrecognized Date Format (Unknown Month){clrm.Fore.RESET}"
            )
            output = "Unrecognized_Date_Format"
        else:
            output = f"{year}-{month:02}-{day:02}_{hours:02}-{minutes}-{seconds}"

    return output


def FormattedSubject(subject: str, /) -> str:
    """"""
    if subject is UNKNOWN_SUBJECT:
        return subject

    if isinstance(subject, header_t):
        subject = str(subject)
    if subject.__len__() == 0:
        return "Empty_Subject"

    current = FormattedText(subject, remove_accents=True, only_longest=True)

    # Leave "_" to avoid pieces = []
    current = INVALID_CHARACTERS_REGX.sub("_", current)
    pieces = current.split("_")
    pieces_lengths = tuple(map(len, pieces))
    where_max = pieces_lengths.index(max(pieces_lengths))

    output = pieces[where_max]
    if output.__len__() > 0:
        output = output[:MAX_SUBJECT_LENGTH].lower()
    else:
        print(f"{clrm.Fore.RED}{subject}: Unrecognized Subject Format{clrm.Fore.RESET}")
        output = "Unrecognized_Subject_Format"

    return output


HEADER_ELEMENTS = {
    "Date": (FormattedDate, UNKNOWN_DATE),
    "From": (FormattedSender, UNKNOWN_SENDER),
    "Subject": (FormattedSubject, UNKNOWN_SUBJECT),
}


clrm.init()

parser = arg_parser_t(
    description="Convert MBOXes into EML messages.", allow_abbrev=False
)
parser.add_argument(
    "--in",
    required=True,
    dest="input",
    help="Mbox or folder containing mboxes; If folder, must not be inside EML folder",
)
parser.add_argument(
    "--out",
    required=True,
    dest="output",
    help="Folder for storing EML messages; Must not be inside mbox folder",
)
parser.add_argument(
    "--blank",
    action="store_true",
    help="Blank run (no EML folders created, no EML files written)",
)

arguments = parser.parse_args()

blank_run = arguments.blank

input_path = path_t(arguments.input)
if input_path.is_dir():
    base_input_path = input_path
    walker = input_path.rglob("*")
elif input_path.is_file():
    base_input_path = input_path.parent
    walker = [input_path]
else:
    base_input_path = None  # Just to silence IDE warning
    walker = None  # Just to silence IDE warning
    print(
        f"{clrm.Fore.RED}{input_path}: Not a path to mbox or folder\n{clrm.Fore.RESET}"
    )
    parser.print_help()
    sstm.exit(-1)

output_path = path_t(arguments.output)
if not output_path.exists():
    if blank_run:
        print(f"Creation of folder: {output_path}")
    else:
        output_path.mkdir(mode=0o700)
elif not output_path.is_dir():
    print(f"{clrm.Fore.RED}{output_path}: Not a folder\n{clrm.Fore.RESET}")
    parser.print_help()
    sstm.exit(-1)
if input_path.is_dir() and (
    output_path.is_relative_to(input_path) or input_path.is_relative_to(output_path)
):
    print(
        f"{clrm.Fore.RED}{input_path}/{output_path}: "
        f"Input path is a subfolder of output path, or the opposite\n{clrm.Fore.RESET}"
    )
    parser.print_help()
    sstm.exit(-1)

for node in walker:
    if node.is_file():
        # Simply produces empty lists for keys and values if not an MBox
        mbox = mbox_t(node)
        relative_path = node.relative_to(base_input_path)
        print(f'MBox "{node}": {mbox.keys().__len__()} message(s)')

        for m_idx, message in enumerate(mbox.values()):
            eml_path = output_path / relative_path

            details = tuple(
                _Format(message.get(_dtl, failobj=_dft))
                for _dtl, (_Format, _dft) in HEADER_ELEMENTS.items()
            )
            eml_name = "-".join(details) + f"-{m_idx}.eml"

            if blank_run:
                print(f"    {eml_path / eml_name}")
            else:
                eml_path.mkdir(mode=0o700, parents=True, exist_ok=True)
                with open(eml_path / eml_name, "wb") as accessor:
                    message_bytes = bytes_io_t()
                    byte_generator = byte_generator_t(message_bytes, maxheaderlen=0)
                    byte_generator.flatten(message)
                    contents = message_bytes.getvalue()
                    accessor.write(contents)
