# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

[ $# -ge 1 ] && mbox="$1" || exit 1
[ $# -ge 2 ] && eml_folder="$2" || exit 2
[ $# -ge 3 ] && option="$3" || option=

[ ! -f "$mbox" ] && { echo "$mbox"': Not a(n MBox) file'; exit 3; }
[ ! -d "$eml_folder" ] && { echo "$eml_folder"': Not a(n EML) folder'; exit 4; }
[ "$option" = '--quiet' ] && option='-q' || option=

rebuilt_mbox=`mktemp`
diff_file=`mktemp`

n_messages=0
while read eml
do
    n_messages=`expr $n_messages + 1`
    name=`echo "$eml" | cut -d '#' -f 2`
    # Add an MBOX message separator line
    echo 'From ' >> "$rebuilt_mbox"
    cat "$eml_folder/$name" >> "$rebuilt_mbox"
done < <(\
    cd "$eml_folder" && \
    ls -1 *.eml \
        | sed -n -e 's/^\(..*\)-\([0-9][0-9]*\)\.eml$/\2#\1-\2.eml/p' \
        | sort -n)

n_originals=`grep -c '^From ' "$mbox"`

diff -wB $option "$mbox" "$rebuilt_mbox" > "$diff_file"
if [ $? = 1 ]
then
    echo 'COMPARING: '"$mbox" "$rebuilt_mbox"
    echo '++++++++++'
    cat "$diff_file"
    echo "Originals: $n_originals / EMLs: $n_messages"
    ls -1s "$mbox" "$rebuilt_mbox"
    echo '----------'
fi
