# Copyright CNRS/Inria/UCA
# Contributor(s): Eric Debreuve (since 2021)
#
# eric.debreuve@cnrs.fr
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import base64 as bs64
import os as osys
import re
import re as regx
import sys as sstm
import tempfile as tmpf
from argparse import ArgumentParser as arg_parser_t
from email import policy
from email.message import Message as message_t
from email.parser import BytesParser as bytes_parser_t
from enum import Enum as enum_t
from pathlib import Path as path_t
from typing import Dict, List
from typing import NamedTuple as named_tuple_t
from typing import Optional, Sequence, Tuple, Union

import colorama as clrm
import poppler as pppl
import PyQt5.QtCore as qtcr
import PyQt5.QtGui as qtgi
import PyQt5.QtWidgets as wdgt

from decoder import FormattedText


HEADER_DETAILS_REGX = regx.compile("(From:)|(Date:)|(Subject:)|(To:)")

CONTENT_BOUNDARY_REGX = regx.compile(
    "Content-Type: *[a-zA-Z/]+; *\n? *"
    "boundary=(--)?(=_)?([ 0-9a-zA-Z]+)(_=)?(--)? *\n"
)
CONTENT_TYPE_REGX = regx.compile(
    "Content-Type: *([a-zA-Z/]+); *\n? *" "charset=([-0-9a-zA-Z]+) *\n"
)
CHARSET_REGX = regx.compile('charset="([^"]+)"')
TEXT_NEWLINES_REGX = re.compile("\n{3,}")
HTML_OPENING_TAG = re.compile("<html>", flags=regx.IGNORECASE)
HTML_CLOSING_TAG = re.compile("</html>", flags=regx.IGNORECASE)
HTML_NEWLINES_REGX = re.compile("(<br */? *>\n?){3,}", flags=regx.IGNORECASE)
HTML_NBSP_REGX = re.compile("&nbsp;", flags=regx.IGNORECASE)

IMAGE_FORMAT_AND_NAME_REGX = regx.compile(
    "Content-Type: *image/([^;]+); *\n? *" 'name="([^"]+)" *\n'
)
PDF_FORMAT_AND_NAME_REGX = regx.compile(
    "Content-Type: *application/(pdf|PDF); *\n? *" 'name="([^"]+)" *\n'
)
FILE_FORMAT_AND_NAME_REGX = regx.compile(
    "Content-Type: *application/([^;]+); *\n? *" 'name="([^"]+)" *\n'
)
ATTACHMENT_FILENAME_REGX = re.compile(
    "Content-Disposition: *attachment; *\n? *" 'filename="([^"]+)" *\n'
)
ATTACHMENT_ENCODING_REGX = regx.compile("Content-Transfer-Encoding: +([^ \n]+) *\n")


P2Q_FORMAT = {
    pppl.ImageFormat.invalid: qtgi.QImage.Format_Invalid,
    pppl.ImageFormat.argb32: qtgi.QImage.Format_ARGB32,
    pppl.ImageFormat.bgr24: qtgi.QImage.Format_BGR888,
    pppl.ImageFormat.gray8: qtgi.QImage.Format_Grayscale8,
    pppl.ImageFormat.mono: qtgi.QImage.Format_Mono,
    pppl.ImageFormat.rgb24: qtgi.QImage.Format_RGB888,
}


MAX_TAB_TITLE_LENGTH = 20


class header_keys_t(enum_t):
    from_ = "From"
    date = "Date"
    subject = "Subject"
    to = "To"


class header_styles_t(named_tuple_t):
    name: str
    should_decode: bool
    color: Optional[str]
    weight: Optional[str]


HEADER_STYLES: Dict[header_keys_t, header_styles_t] = {
    header_keys_t.from_: header_styles_t(
        name="Sender", should_decode=True, color="green", weight=None
    ),
    header_keys_t.date: header_styles_t(
        name="Date", should_decode=False, color=None, weight=None
    ),
    header_keys_t.subject: header_styles_t(
        name="Subject", should_decode=True, color="blue", weight="bold"
    ),
    header_keys_t.to: header_styles_t(
        name="Addressee(s)", should_decode=True, color=None, weight=None
    ),
}


class viewer_t:
    def __init__(self, path: path_t, /) -> None:
        """"""
        app = wdgt.QApplication(sstm.argv)
        layout = wdgt.QGridLayout()

        if path.is_file():
            window = wdgt.QWidget()
            window.setLayout(layout)
            eml_list_wgt = None
            ViewDefaultMessage = lambda: self.ViewMessageAtPath(path)
        else:
            window = wdgt.QSplitter()

            path_wgt = wdgt.QWidget()
            path_lyt = wdgt.QVBoxLayout()
            path_wgt.setLayout(path_lyt)

            path_btn = wdgt.QPushButton("Change Folder")
            path_btn.pressed.connect(self.UpdateMessageList)
            path_lyt.addWidget(path_btn)

            eml_list_wgt = wdgt.QListWidget()
            for node in sorted(path.glob("*")):
                if node.is_file() and not node.name.startswith("."):
                    eml_list_wgt.addItem(node.name)
            eml_list_wgt.currentTextChanged.connect(self.UpdateMessageWithName)
            path_lyt.addWidget(eml_list_wgt)
            ViewDefaultMessage = lambda: eml_list_wgt.setCurrentRow(0)

            message_wgt = wdgt.QWidget()
            message_wgt.setLayout(layout)

            window.addWidget(path_wgt)
            window.addWidget(message_wgt)
        window.setWindowTitle("EML view")

        self.app = app
        self.window = window
        self.layout = layout
        self.eml_list_wgt = eml_list_wgt
        self.header_wgts = None
        self.content_wgt = None
        self.path = path

        self._CreateHeaderWidgets()
        ViewDefaultMessage()

    def _CreateHeaderWidgets(self) -> None:
        """"""
        self.header_wgts = {}
        layout = wdgt.QGridLayout()

        for e_idx, style in enumerate(HEADER_STYLES.values(), start=1):
            name = NewSelectableQLabel(style.name, variant="italic")
            content = NewSelectableQLabel("", color=style.color, weight=style.weight)
            name.setSizePolicy(wdgt.QSizePolicy.Fixed, wdgt.QSizePolicy.Minimum)
            layout.addWidget(name, e_idx, 1)
            layout.addWidget(content, e_idx, 2)

            self.header_wgts[style.name] = content

        self.layout.addLayout(layout, 1, 1)

    def SetHeaderFromMessage(self, message: message_t, /) -> None:
        """"""
        for e_idx, (key, style) in enumerate(HEADER_STYLES.items(), start=1):
            content = message.get(key.value, failobj="Unknown " + style.name)
            if style.should_decode:
                content = FormattedText(content)
            if key is header_keys_t.to:
                addressees = (_add.strip() for _add in content.split(","))
                content = "\n".join(addressees)

            self.header_wgts[style.name].setText(content)

    def AddContentWidget(self, content_versions: Sequence[Tuple[str, str]], /) -> None:
        """"""
        if (n_versions := content_versions.__len__()) > 0:
            if n_versions > 1:
                version_wgt = wdgt.QTabWidget()
                for version in content_versions:
                    tab_title, widget = NewContentWidget(*version)
                    if tab_title.__len__() > MAX_TAB_TITLE_LENGTH:
                        tab_title = tab_title[: (MAX_TAB_TITLE_LENGTH - 3)] + "..."
                    version_wgt.addTab(widget, tab_title)
            else:
                _, version_wgt = NewContentWidget(*content_versions[0])
        else:
            version_wgt = NewSelectableQLabel("Empty Content")

        scroller = wdgt.QScrollArea()
        scroller.setWidget(version_wgt)

        if self.content_wgt is None:
            self.layout.addWidget(scroller, 2, 1)
        else:
            self.layout.replaceWidget(self.content_wgt, scroller)
            self.content_wgt.setParent(None)
        self.content_wgt = scroller

    def ViewMessageAtPath(self, path: path_t, /) -> None:
        """"""
        with open(path, "rb") as accessor:
            # policy=policy.default turns the message from email.message.Message to email.message.EmailMessage
            message = bytes_parser_t(policy=policy.default).parse(accessor)
        content_versions = ContentVersions(message)

        self.SetHeaderFromMessage(message)
        self.AddContentWidget(content_versions)

    def UpdateMessageWithName(self, name: str, /) -> None:
        """"""
        self.ViewMessageAtPath(self.path / name)

    def UpdateMessageList(self) -> None:
        """"""
        dialog = wdgt.QFileDialog(self.window, "Select an EML folder", str(self.path))
        dialog.setFileMode(wdgt.QFileDialog.Directory)
        dialog.setAcceptMode(wdgt.QFileDialog.AcceptOpen)
        if dialog.exec():
            paths = dialog.selectedFiles()
            path = path_t(paths[0])
            self.path = path

            eml_list_wgt = self.eml_list_wgt
            # To prevent callback calls on clearing or re-populating
            eml_list_wgt.blockSignals(True)
            eml_list_wgt.clear()
            for node in sorted(path.glob("*")):
                if node.is_file() and not node.name.startswith("."):
                    eml_list_wgt.addItem(node.name)
            eml_list_wgt.blockSignals(False)
            eml_list_wgt.setCurrentRow(0)

    def Activate(self) -> None:
        """"""
        close = wdgt.QPushButton("Close")
        close.clicked.connect(wdgt.qApp.exit)
        self.layout.addWidget(close, 3, 1)

        self.window.show()

        sstm.exit(self.app.exec_())


class image_viewer_t(wdgt.QWidget):
    def __init__(self, path: str, pixmap: qtgi.QPixmap, /) -> None:
        """"""
        super().__init__()
        layout = wdgt.QGridLayout()
        self.setLayout(layout)

        save_btn = wdgt.QPushButton("Save As")
        save_btn.setSizePolicy(wdgt.QSizePolicy.Expanding, wdgt.QSizePolicy.Fixed)
        save_btn.pressed.connect(self.SaveAs)

        save_png_btn = wdgt.QPushButton("Save in PNG As")
        save_png_btn.setSizePolicy(wdgt.QSizePolicy.Expanding, wdgt.QSizePolicy.Fixed)
        save_png_btn.pressed.connect(self.SaveAsPNG)

        image_wgt = NewSelectableQLabel(pixmap)

        layout.addWidget(save_btn, 1, 1)
        layout.addWidget(save_png_btn, 1, 2)
        layout.addWidget(image_wgt, 2, 1, 1, 2)

        self.path = path_t(path)
        self.image_wgt = image_wgt

    def SaveAs(self) -> None:
        """"""
        path = RequestedStoragePath(str(path_t.home()), self.window())
        if path is not None:
            path.write_bytes(self.path.read_bytes())

    def SaveAsPNG(self) -> None:
        """"""
        path = RequestedStoragePath(str(path_t.home()), self.window())
        if path is not None:
            pixmap = self.image_wgt.pixmap()
            pixmap.save(str(path), "PNG")


class pdf_viewer_t(wdgt.QWidget):
    def __init__(self, path: str, /) -> None:
        """"""
        super().__init__()
        layout = wdgt.QGridLayout()
        self.setLayout(layout)

        self.document = pppl.load_from_file(path)

        try:
            self.n_pages = self.document.pages
        except AttributeError:
            self.document = None

        page_wgt = wdgt.QLabel()
        if self.document is None:
            page_wgt.setText("Unreadable PDF document")
            layout.addWidget(page_wgt, 1, 1)
        else:
            page_number_wgt = wdgt.QLabel()
            page_number_wgt.setAlignment(qtcr.Qt.AlignCenter)
            previous_btn = wdgt.QPushButton("<-")
            next_btn = wdgt.QPushButton("->")
            previous_btn.pressed.connect(lambda: self.ShowPage(self.current_page - 1))
            next_btn.pressed.connect(lambda: self.ShowPage(self.current_page + 1))
            save_btn = wdgt.QPushButton("Save As")
            save_btn.pressed.connect(self.SaveAs)

            layout.addWidget(previous_btn, 1, 1)
            layout.addWidget(page_number_wgt, 1, 2)
            layout.addWidget(next_btn, 1, 3)
            layout.addWidget(save_btn, 1, 4)
            layout.addWidget(page_wgt, 2, 1, 1, 4)

            self.current_page = None
            self.renderer = pppl.PageRenderer()
            self.page_wgt = page_wgt
            self.page_number_wgt = page_number_wgt
            self.path = path_t(path)

            self.ShowPage(0)

    def ShowPage(self, index: int, /) -> None:
        """"""
        if index < 0:
            return
        elif index >= self.n_pages:
            return

        page = self.document.create_page(index)
        image = self.renderer.render_page(page)
        q_image = qtgi.QImage(
            image.data,
            image.width,
            image.height,
            image.bytes_per_row,
            P2Q_FORMAT[image.format],
        )
        pixmap = qtgi.QPixmap.fromImage(q_image)

        self.current_page = index
        self.page_wgt.setPixmap(pixmap)
        self.page_number_wgt.setText(str(index + 1))

    def SaveAs(self) -> None:
        """"""
        path = RequestedStoragePath(str(path_t.home()), self.window())
        if path is not None:
            path.write_bytes(self.path.read_bytes())


class storable_t(wdgt.QWidget):
    def __init__(self, path: str, name: str, /) -> None:
        """"""
        super().__init__()
        layout = wdgt.QVBoxLayout()
        self.setLayout(layout)

        name_wgt = NewSelectableQLabel(name)
        name_wgt.setSizePolicy(wdgt.QSizePolicy.Expanding, wdgt.QSizePolicy.Fixed)

        save_btn = wdgt.QPushButton("Save As")
        save_btn.setSizePolicy(wdgt.QSizePolicy.Expanding, wdgt.QSizePolicy.Fixed)
        save_btn.pressed.connect(self.SaveAs)

        layout.addWidget(name_wgt)
        layout.addWidget(save_btn)
        layout.addStretch(1)

        self.path = path_t(path)

    def SaveAs(self) -> None:
        """"""
        path = RequestedStoragePath(str(path_t.home()), self.window())
        if path is not None:
            path.write_bytes(self.path.read_bytes())


def EMFPath() -> path_t:
    """"""
    parser = arg_parser_t(description="View EML messages.", allow_abbrev=False)
    parser.add_argument(
        "eml_path",
        help="Path to an EML file or folder",
    )

    arguments = parser.parse_args()

    output = path_t(arguments.eml_path)
    if not (output.is_file() or output.is_dir()):
        print(
            f"{clrm.Fore.RED}{output}: Not a path to a(n EML) file or folder\n{clrm.Fore.RESET}"
        )
        parser.print_help()
        sstm.exit(-1)

    return output


def PartWithType(
    message: message_t, payload_idx: int, /
) -> Tuple[Optional[message_t], Optional[str], Optional[str], int]:
    """"""
    part = None
    part_type = None
    part_as_string = None
    # 0: exit loop immediately
    # 1: finish loop and exit
    # >1: continue looping
    n_remaining_iterations = 2

    if message.is_multipart():
        try:
            part = message.get_payload(payload_idx)
            part_type = part.get_content_type().lower()
        except IndexError:
            n_remaining_iterations = 0
    else:
        part = message.get_payload()
        part_type = "text/---"
        n_remaining_iterations = 1

    if isinstance(part, str):
        part_as_string = part.strip()
    elif part is not None:
        part_as_string = part.as_string().strip()

    return part, part_type, part_as_string, n_remaining_iterations


def HeaderFromContent(content: str, /) -> str:
    """"""
    header_details = []

    for line in content.splitlines():
        if HEADER_DETAILS_REGX.match(line):
            header_details.append(line)

    return "\n".join(header_details)


def ContentVersions(message: message_t, /) -> Sequence[Tuple[str, str]]:
    """"""
    output = []

    p_idx = 0
    should_continue = True
    while should_continue:
        elements = PartWithType(message, p_idx)
        part, part_type, part_as_string, n_remaining_iterations = elements

        if n_remaining_iterations == 0:
            break
        elif n_remaining_iterations == 1:
            should_continue = False
        else:
            p_idx += 1

        if part_type in ("text/plain", "text/html", "text/---"):
            if part_type == "text/plain":
                content = ContentForPlain(part, part_as_string)
            elif part_type == "text/html":
                content = ContentForHTML(part_as_string)
            else:
                header, content = ContentForOtherTypes(part_as_string)
                if header is not None:
                    content.append(("HEADER", HeaderFromContent(header)))

            if isinstance(content, str):
                content = DecodedContent(part, content)
                content = CleanedContent(content)
                if content.__len__() > 0:
                    output.append((part_type, content))
            else:
                output.extend(content)
        elif part_type.startswith("application/") or part_type.startswith("image/"):
            output.append((part_type, part_as_string))
        else:
            content = CleanedContent(part_as_string)
            if content.__len__() > 0:
                output.append((part_type, content))

    return output


def ContentForPlain(part: message_t, part_as_string: str, /) -> str:
    """"""
    c_idx = IndexAfterDetailInPart(part, part_as_string, "Content-Type")
    t_idx = IndexAfterDetailInPart(part, part_as_string, "Content-Transfer-Encoding")
    content_start = max(c_idx, t_idx)
    if content_start < 0:
        content_start = 0

    return part_as_string[content_start:]


def ContentForHTML(part_as_string: str, /) -> str:
    """"""
    if (match := HTML_OPENING_TAG.search(part_as_string)) is None:
        output = "<html>" + part_as_string
    else:
        output = part_as_string[match.start(0) :]

    if (match := HTML_CLOSING_TAG.search(output)) is None:
        output = output + "</html>"
    else:
        output = output[:match.end(0)]

    output = HTML_NBSP_REGX.sub(" ", output)
    output = CleanedContent(output)
    output = HTML_NEWLINES_REGX.sub("<br><br>", output)

    return output


def ContentForOtherTypes(
    part_as_string: str, /
) -> Tuple[Optional[str], List[Tuple[str, str]]]:
    """
    Content-Type: multipart/alternative; boundary=089e013cb99223b3440509c40293

    --089e013cb99223b3440509c40293
    Content-Type: text/plain; charset=UTF-8
    """
    header = None
    content_versions = []

    if (match := CONTENT_BOUNDARY_REGX.search(part_as_string)) is not None:
        boundary = match.group(3)

        header_end = IndexOfPreviousNewlineCharacter(part_as_string, match.start(3))
        header = part_as_string[:header_end].strip()
        if header.__len__() == 0:
            header = None

        search_start = match.end(3)
        should_continue = True
        while should_continue:
            boundary_idx = part_as_string.find(boundary, search_start)
            if boundary_idx < 0:
                content = part_as_string[search_start:]
                content_versions.append(("text/---", content))
                should_continue = False
            else:
                content_start = IndexOfNextLine(part_as_string, boundary_idx)
                next_boundary_idx = part_as_string.find(boundary, content_start)
                if next_boundary_idx < 0:
                    content_end = part_as_string.__len__()
                    should_continue = False
                else:
                    content_end = IndexOfPreviousNewlineCharacter(
                        part_as_string, next_boundary_idx
                    )
                    # Do not add boundary_length since it must be found around content
                    search_start = next_boundary_idx

                content = part_as_string[content_start:content_end]
                if (match := CONTENT_TYPE_REGX.search(content)) is None:
                    content_versions.append(("text/---", content))
                else:
                    content_type = match.group(1)
                    charset = match.group(2)
                    content = content[match.end(2) :]
                    content = DecodedContentFromCharset(charset, content)
                    content_versions.append((content_type, content))

    if content_versions.__len__() == 0:
        if header is None:
            content_start = 0
        else:
            content_start = header.__len__()
        content = part_as_string[content_start:]
        content_versions = [("text/---", content)]

    cleaned_versions = []
    for content_type, content in content_versions:
        cleaned = CleanedContent(content)
        if cleaned.__len__() > 0:
            cleaned_versions.append((content_type, cleaned))

    return header, cleaned_versions


def DecodedContentFromCharset(charset: str, content: str, /) -> str:
    """"""
    lines = []
    for line in content.splitlines():
        line = FormattedText(f"=?{charset}?q?" + line + "?=")
        lines.append(line)

    return "\n".join(lines)


def DecodedContent(part: message_t, content: str, /) -> str:
    """"""
    output = content

    if (content_type := part.get("Content-Type")) is not None:
        if (match := CHARSET_REGX.search(content_type)) is not None:
            charset = match.group(1)
            output = DecodedContentFromCharset(charset, content)

    return output


def DecodedAttachment(
    attachment: str, format_and_name_regx: regx.Pattern, /
) -> Tuple[str, str]:
    """
    Content-Type: image/jpeg; name="photo.JPG"
    Content-Disposition: attachment; filename="photo.JPG"
    Content-Transfer-Encoding: base64
    """
    match = format_and_name_regx.search(attachment)
    if match is None:
        _ = None  # format
        match = ATTACHMENT_FILENAME_REGX.search(attachment)
        if match is None:
            name = None
            from_idx_1 = 0
        else:
            name = match.group(1)
            from_idx_1 = match.end(1)
    else:
        _ = match.group(1)  # format
        name = match.group(2)
        from_idx_1 = match.end(2)

    match = ATTACHMENT_ENCODING_REGX.search(attachment)
    if match is None:
        encoding = None
        from_idx_2 = 0
    else:
        encoding = match.group(1)
        from_idx_2 = match.end(1)

    stream_start = max(from_idx_1, from_idx_2)
    attachment = attachment[stream_start:].strip()
    attachment = "".join(attachment.splitlines())
    attachment = bytes(attachment, "UTF-8")
    if encoding == "base64":
        try:
            attachment = bs64.b64decode(attachment)
        except:
            pass

    accessor, path = tmpf.mkstemp()
    osys.write(accessor, attachment)
    osys.close(accessor)

    return name, path


def DecodedImage(image: str, img_format: str, /) -> Tuple[str, str, qtgi.QPixmap]:
    """
    Content-Type: image/jpeg; name="photo.JPG"
    Content-Disposition: attachment; filename="photo.JPG"
    Content-Transfer-Encoding: base64
    """
    name, path = DecodedAttachment(image, IMAGE_FORMAT_AND_NAME_REGX)
    if name is None:
        name = f"image/{img_format.upper()}"
    pixmap = qtgi.QPixmap(path, img_format)

    return name, path, pixmap


def IndexAfterDetailInPart(part: message_t, part_as_string: str, detail: str, /) -> int:
    """"""
    value = part.get(detail)
    if value is None:
        output = -1
    else:
        idx = part_as_string.find(value)
        if idx >= 0:
            idx = IndexOfNextLine(part_as_string, idx + value.__len__())
        output = idx

    return output


def IndexOfNextLine(content: str, start_idx: int, /) -> int:
    """"""
    output = content.find("\n", start_idx)
    if output < 0:
        output = content.__len__()
    else:
        output += 1

    return output


def IndexOfPreviousNewlineCharacter(content: str, end_idx: int, /) -> int:
    """"""
    output = content.rfind("\n", 0, end_idx+1)
    if output < 0:
        output = 0

    return output


def CleanedContent(content: str, /) -> str:
    """"""
    output = []

    for line in content.splitlines():
        output.append(line.strip())

    output = "\n".join(output)
    output = TEXT_NEWLINES_REGX.sub("\n\n", output)

    return output.strip()


def NewSelectableQLabel(
    content: Union[str, qtgi.QPixmap],
    /,
    *,
    color: str = None,
    weight: str = None,
    variant: str = None,
) -> wdgt.QLabel:
    """"""
    if isinstance(content, str):
        output = wdgt.QLabel(content)
    else:
        output = wdgt.QLabel()
        output.setPixmap(content)
    output.setAlignment(qtcr.Qt.AlignTop)
    style = []
    if color is not None:
        style.append(f"color: {color}")
    if weight:
        style.append(f"font-weight: {weight}")
    if variant:
        style.append(f"font-style: {variant}")
    if style.__len__() > 0:
        output.setStyleSheet(";".join(style))

    output.setTextInteractionFlags(qtcr.Qt.TextSelectableByMouse)

    return output


def NewContentWidget(content_type: str, content: str) -> Tuple[str, wdgt.QWidget]:
    """"""
    if content_type.startswith("image/"):
        name, path, pixmap = DecodedImage(content, content_type[6:])
        widget = image_viewer_t(path, pixmap)
    elif content_type == "application/pdf":
        name, widget = NewPDFViewer(content)
    elif content_type.startswith("application/"):
        name, widget = NewStorableDocumentWidget(content, content_type[12:])
    else:
        name = content_type
        widget = NewSelectableQLabel(content)
        widget.setWordWrap(True)

    return name, widget


def NewPDFViewer(pdf: str, /) -> Tuple[str, pdf_viewer_t]:
    """
    Content-Type: application/pdf; name="document.PDF"
    Content-Disposition: attachment; filename="document.PDF"
    Content-Transfer-Encoding: base64
    """
    name, path = DecodedAttachment(pdf, PDF_FORMAT_AND_NAME_REGX)
    if name is None:
        name = "text/PDF"

    return name, pdf_viewer_t(path)


def NewStorableDocumentWidget(
    document: str, doc_format: str, /
) -> Tuple[str, storable_t]:
    """
    Content-Type: application/???; name="document.???"
    Content-Disposition: attachment; filename="document.???"
    Content-Transfer-Encoding: base64
    """
    name, path = DecodedAttachment(document, FILE_FORMAT_AND_NAME_REGX)
    if name is None:
        name = f"doc/{doc_format.upper()}"

    return name, storable_t(path, name)


def RequestedStoragePath(
    initial_folder: str, parent: wdgt.QWidget, /
) -> Optional[path_t]:
    """"""
    dialog = wdgt.QFileDialog(parent, "Save Document As", initial_folder)
    dialog.setAcceptMode(wdgt.QFileDialog.AcceptSave)
    if dialog.exec():
        paths = dialog.selectedFiles()
        output = path_t(paths[0])
    else:
        output = None

    return output


if __name__ == "__main__":
    #
    clrm.init()
    viewer = viewer_t(EMFPath())
    viewer.Activate()
